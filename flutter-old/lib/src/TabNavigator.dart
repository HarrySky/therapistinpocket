import 'package:flutter/material.dart';
import 'NavBar.dart';
import 'sections/sections.dart';

class TabNavigatorRoutes {
  static const String profile = '/profile';
  static const String home = '/';
  static const String section = '/section';
  static const String settings = '/settings';
  static const String search = '/search';
}

Map<TabItem, String> tabInitialRoute = {
  TabItem.profile: TabNavigatorRoutes.profile,
  TabItem.home: TabNavigatorRoutes.home,
  TabItem.settings: TabNavigatorRoutes.settings,
  TabItem.search: TabNavigatorRoutes.search
};

class TabNavigator extends StatelessWidget {
  TabNavigator({this.navigatorKey, this.tabItem});
  final GlobalKey<NavigatorState> navigatorKey;
  final TabItem tabItem;

  void _pushSection(BuildContext context, {SectionItem section}) {
    var routeBuilders = _routeBuilders(context, section: section);

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => routeBuilders[TabNavigatorRoutes.section](context),
      ),
    );
  }

  Map<String, WidgetBuilder> _routeBuilders(
    BuildContext context, {SectionItem section}
  ) {
    return {
      TabNavigatorRoutes.profile: (context) => Container(color: Colors.green[100]),
      TabNavigatorRoutes.home: (context) => Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          title: Text(tabName[tabItem])
        ),
        body: Container(
          height: 3000.0,
          child: ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.black,
            ),
            itemCount: SectionItem.values.length,
            itemBuilder: (context, index) => 
              SectionTile(SectionItem.values[index], _pushSection)
          ),
        ),
      ),
      TabNavigatorRoutes.section: (context) => sectionBuilder[section](context),
      TabNavigatorRoutes.settings: (context) => Container(color: Colors.green[300]),
      TabNavigatorRoutes.search: (context) => Container(color: Colors.green[400]),
    };
  }

  @override
  Widget build(BuildContext context) {
    final routeBuilders = _routeBuilders(context);
    final initialRoute = tabInitialRoute[tabItem];

    return Navigator(
      key: navigatorKey,
      initialRoute: initialRoute,
      onGenerateRoute: (routeSettings) {
        return MaterialPageRoute(
          builder: (context) => routeBuilders[routeSettings.name](context),
        );
      },
    );
  }
}

class SectionTile extends StatelessWidget {
  final SectionItem section;
  final void Function(BuildContext context, {SectionItem section}) push;
  final textStyle = const TextStyle(fontSize: 24.0);// , backgroundColor: Colors.blue);

  SectionTile(this.section, this.push);

  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        height: 200,
        color: Colors.amberAccent,
        child: Center(child: Text(sectionName[section], style: textStyle),),
      ),
      onTap: () {
        push(context, section: section);
      },
    );
  }
}
