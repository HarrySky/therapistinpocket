import 'package:flutter/material.dart';

enum TabItem {
  profile,
  home,
  settings,
  search
}

Map<TabItem, String> tabName = {
  TabItem.profile: 'Profile',
  TabItem.home: 'Home',
  TabItem.settings: 'Settings',
  TabItem.search: 'Search'
};

Map<TabItem, Icon> tabIcon = {
  TabItem.profile: Icon(Icons.account_circle),
  TabItem.home: Icon(Icons.home),
  TabItem.settings: Icon(Icons.settings),
  TabItem.search: Icon(Icons.search)
};

class NavBar extends StatelessWidget {
  NavBar({this.currentTab, this.onSelectTab});

  final TabItem currentTab;
  final ValueChanged<TabItem> onSelectTab;

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      currentIndex: currentTab.index,
      items: [
        _buildItem(tabItem: TabItem.profile),
        _buildItem(tabItem: TabItem.home),
        _buildItem(tabItem: TabItem.settings),
        _buildItem(tabItem: TabItem.search),
      ],
      onTap: (index) => onSelectTab(
        TabItem.values[index],
      ),
      backgroundColor: Colors.white,
      selectedItemColor: Theme.of(context).primaryColor,
      unselectedItemColor: Colors.grey,
    );
  }

  BottomNavigationBarItem _buildItem({TabItem tabItem}) {
    String text = tabName[tabItem];
    Icon icon = tabIcon[tabItem];
    return BottomNavigationBarItem(
      icon: icon,
      title: Text(text),
    );
  }
}
