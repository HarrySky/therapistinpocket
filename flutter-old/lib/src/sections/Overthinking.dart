import 'package:flutter/material.dart';

class Overthinking  extends StatelessWidget {
  static String routeName = '/sections/Overthinking';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Overthinking',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Overthinking'),
        ),
        body: Center(
          child: Text('Hello World'),
        ),
      ),
    );
  }
}