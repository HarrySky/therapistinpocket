import 'package:flutter/material.dart';
import 'Anxiety.dart';
import 'Fear.dart';
import 'Overthinking.dart';
import 'Depression.dart';

enum SectionItem {
  anxiety,
  fear,
  overthinking,
  depression
}

Map<SectionItem, String> sectionName = {
  SectionItem.anxiety: 'Anxiety',
  SectionItem.fear: 'Fear',
  SectionItem.overthinking: 'Overthinking',
  SectionItem.depression: 'Depression'
};

Map<SectionItem, WidgetBuilder> sectionBuilder = {
  SectionItem.anxiety: (context) => Anxiety(),
  SectionItem.fear: (context) => Fear(),
  SectionItem.overthinking: (context) => Overthinking(),
  SectionItem.depression: (context) => Depression()
};
