import React from 'react'
import AppTabs from './components/AppTabs'
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'
import OverthinkingPage from './components/OverthinkingPage'
import HomePage from './components/HomePage'
import './App.css'
import { createMuiTheme } from '@material-ui/core'
import { deepPurple, blue } from '@material-ui/core/colors'
import { MuiThemeProvider } from '@material-ui/core/styles'
import OverthinkingChapter1Page from './components/OverthinkingCh1Page'

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: deepPurple[700]
    },
    secondary: {
      main: blue[100]
    },
    type: 'light'
  }
})

function App () {
  return (
    <MuiThemeProvider theme={theme}>
      <Router>
        <Switch>
          <Route exact path='/' component={HomePage} />
          <Route exact path='/overthinking' component={OverthinkingPage} />
          <Route exact path='/overthinking/1' component={OverthinkingChapter1Page} />
        </Switch>
        <AppTabs />
      </Router>
    </MuiThemeProvider>
  )
}

export default App
