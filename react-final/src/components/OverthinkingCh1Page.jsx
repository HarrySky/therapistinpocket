import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import { Typography, Button } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  chapterCard: {
    width: '100%',
    height: '100px',
    backgroundColor: theme.palette.primary.main,
    color: 'white',
    position: 'fixed',
    top: 0,
    left: 0,
    zIndex: 100
  },
  text: {
    margin: 'auto 30px auto auto',
    textAlign: 'right',
    fontFamily: "'Overpass', sans-serif",
    fontWeight: '300',

    lineHeight: '100px',
    position: 'relative',
    zIndex: 1
  },
  whiteLine: {
    height: 90,
    borderBottom: 'solid 5px white',
    marginLeft: 10,
    width: '80%',
    top: -110,
    position: 'relative'
  },
  content: {
    padding: '10px',
    paddingTop: '110px',
    textAlign: 'center'
  },
  sectionCard: {
    marginTop: '10px',
    marginBottom: '10px',
    width: '100%',
    height: '80px',
    backgroundColor: theme.palette.secondary.main,
    color: 'black',
    position: 'relative'
  },
  sectionLabel: {
    textAlign: 'center',
    fontFamily: "'Overpass', sans-serif",
    fontWeight: '300',

    lineHeight: '100px',
    position: 'relative',
    zIndex: 1
  },
  blackLine: {
    height: 90,
    borderBottom: 'solid 5px black',
    marginLeft: '5%',
    width: '90%',
    top: -120,
    position: 'relative'
  },
  iframeContainer: {
    overflow: 'hidden',
    paddingTop: '56.25%',
    position: 'relative'
  }
}))

const OverthinkingChapter1Page = () => {
  const classes = useStyles()

  return (
    <>
      <Card className={classes.chapterCard}>
        <Typography variant='h4' className={classes.text}>
          Chapter 1
        </Typography>
        <div className={classes.whiteLine} />
      </Card>
      <div className={classes.content}>
        <Typography variant='subtitle1' gutterBottom>
        Chapter 1: Empty space
          <br />
        Overthinking comes when a person has formed this as a habit or too many things are happening in life. Solutions to this are numerous.
          <br />
          <br />
        There are times when the cup gets too full and there is no empty space inside. In these moments, too much is happening in life: too many emotions, too much that you can't change, though you would like...
        </Typography>
        <Button
          color='primary'
          variant='contained'
          href='https://drive.google.com/open?id=1yksAsmc07YPj12YLX3poT2rfbHRwBE2U983CAFOCB4c'
        >
          Get PDF
        </Button>

        <Card className={classes.sectionCard}>
          <Typography variant='h4' className={classes.sectionLabel}>
            Video
          </Typography>
          <div className={classes.blackLine} />
        </Card>
        <div className={classes.iframeContainer}>
          <iframe
            style={{
              height: '100%',
              left: 0,
              position: 'absolute',
              top: 0,
              width: '100%'
            }}
            title='Overthinking Video 1'
            src='https://www.youtube.com/embed/TOROQ9uq-tw'
            frameborder='0'
            allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'
            allowfullscreen
          />
        </div>

        <Card className={classes.sectionCard}>
          <Typography variant='h4' className={classes.sectionLabel}>
            Audio
          </Typography>
          <div className={classes.blackLine} />
        </Card>
        <Button
          style={{
            marginBottom: '5px'
          }}
          color='primary'
          variant='outlined'
          href='https://drive.google.com/open?id=1s4gU1hZSuz3j_pCOFbMXq6F8zlnAbY11'
        >
          Meditation 1
        </Button>
        <br />
        <Button color='primary' variant='outlined' href='https://drive.google.com/open?id=1bHaomQPz7T28FUmnrCWbcF-xwjciKs1S'>
          Meditation 2
        </Button>
      </div>
    </>
  )
}

export default OverthinkingChapter1Page
