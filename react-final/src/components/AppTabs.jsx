import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import CssBaseline from '@material-ui/core/CssBaseline'
import HomeIcon from '@material-ui/icons/Home'
import {
  Link
} from 'react-router-dom'
import { Tabs, Tab } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  appBar: {
    top: 'auto',
    bottom: 0,
    backgroundColor: 'white',
    height: '72px'
  }
}))

export default function AppTabs () {
  const classes = useStyles()
  const [value, setValue] = React.useState(0)

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  return (
    <>
      <CssBaseline />
      <AppBar position='fixed' className={classes.appBar}>
        <Tabs
          variant='fullWidth'
          value={value}
          onChange={handleChange}
          aria-label='Navigation'
          indicatorColor='primary'
          textColor='primary'
        >
          <Tab
            icon={<HomeIcon />}
            label='Home'
            component={Link}
            to='/'
          />
        </Tabs>
      </AppBar>
    </>
  )
}
