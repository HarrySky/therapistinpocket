import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import { Typography } from '@material-ui/core'
import { Link } from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
  chapterCard: {
    width: '160px',
    height: '160px',
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.text.primary,
    margin: 'auto'
  },
  disabledChapterCard: {
    width: '160px',
    height: '160px',
    backgroundColor: theme.palette.grey[200],
    color: theme.palette.text.primary,
    margin: 'auto'
  },
  text: {
    margin: '30px 50px auto auto',
    textAlign: 'right',
    fontFamily: "'Overpass', sans-serif",
    fontWeight: '300',

    lineHeight: '140px',
    position: 'relative',
    zIndex: 1
  },
  whiteLine: {
    height: '80px',
    marginLeft: 10,
    width: '120px',
    borderBottom: 'solid 5px white',
    borderRight: 'solid 5px white',
    top: -120,
    position: 'relative'
  },
  blackLine: {
    height: '80px',
    marginLeft: 10,
    width: '120px',
    borderBottom: 'solid 5px black',
    borderRight: 'solid 5px black',
    top: -120,
    position: 'relative'
  }
}))

export default function ChapterCard ({
  chapter
}) {
  const classes = useStyles()

  return (
    <Link to={chapter === 1 ? '/overthinking/1' : '#'}>
      <Card className={chapter === 1 ? classes.chapterCard : classes.disabledChapterCard}>
        <Typography variant='subtitle1' className={classes.text}>
          Chapter {chapter}
        </Typography>
        <div className={chapter === 1 ? classes.blackLine : classes.whiteLine} />
      </Card>
    </Link>
  )
}
