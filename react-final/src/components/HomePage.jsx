import React from 'react'
import BigCard from './BigCard'
import { makeStyles } from '@material-ui/core/styles'
import { Typography } from '@material-ui/core'

const useStyles = makeStyles({
  container: {
    margin: 'auto',
    padding: '10px'
  },
  title: {
    fontFamily: "'Overpass', sans-serif",
    fontWeight: '200',
    fontStyle: 'italic',
    marginTop: '10px',
    marginBottom: '20px'
  }
})

const HomePage = () => {
  const classes = useStyles()
  return (
    <div className={classes.container}>
      <Typography variant='h5' align='center' className={classes.title}>
        I am here to help you with:
      </Typography>

      <BigCard type='overthinking' />
      <BigCard type='depression' />
      <BigCard type='fear' />
      <BigCard type='anxiety' />
    </div>
  )
}

export default HomePage
