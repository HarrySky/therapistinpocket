import React from 'react'
import ChapterCard from './ChapterCard'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import { Typography } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  overthinkingCard: {
    width: '100%',
    height: '120px',
    backgroundColor: theme.palette.primary.main,
    color: 'white',
    position: 'fixed',
    top: 0,
    left: 0,
    zIndex: 100
  },
  text: {
    margin: 'auto 30px auto auto',
    textAlign: 'right',
    fontFamily: "'Overpass', sans-serif",
    fontWeight: '300',

    lineHeight: '150px',
    position: 'relative',
    zIndex: 1
  },
  whiteLine: {
    height: 95,
    borderBottom: 'solid 5px white',
    marginLeft: 10,
    width: '80%',
    top: -140,
    position: 'relative'
  },
  grid: {
    paddingTop: '140px',
    paddingRight: '10px',
    display: 'grid',
    gridGap: '10px',
    gridTemplateColumns: '50% 50%'
  }
}))

const OverthinkingPage = () => {
  const classes = useStyles()

  return (
    <>
      <Card className={classes.overthinkingCard}>
        <Typography variant='h4' className={classes.text}>
          OVERTHINKING
        </Typography>
        <div className={classes.whiteLine} />
      </Card>
      <div className={classes.grid}>
        <ChapterCard chapter={1} />
        <ChapterCard chapter={2} />
        <ChapterCard chapter={3} />
        <ChapterCard chapter={4} />
        <ChapterCard chapter={5} />
        <ChapterCard chapter={6} />
      </div>
    </>
  )
}

export default OverthinkingPage
