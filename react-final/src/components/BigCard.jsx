import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import {
  Link
} from 'react-router-dom'
import { Typography } from '@material-ui/core'

const base = {
  width: 275,
  height: 150,
  margin: '10px auto'
}

const useStyles = makeStyles((theme) => ({
  anxiety: {
    ...base,
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.text.primary
  },
  depression: {
    ...base,
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.text.primary
  },
  fear: {
    ...base,
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.text.primary
  },
  overthinking: {
    ...base,
    backgroundColor: theme.palette.primary.main,
    color: 'white'
  },
  text: {
    margin: 'auto 50px auto auto',
    textAlign: 'right',
    fontFamily: "'Overpass', sans-serif",
    fontWeight: '300',

    lineHeight: '150px',
    position: 'relative',
    zIndex: 1
  },
  whiteLine: {
    height: 95,
    borderBottom: 'solid 5px white',
    marginLeft: 10,
    width: 200,
    top: -150,
    position: 'relative'
  },
  blackLine: {
    height: 95,
    borderBottom: 'solid 5px black',
    marginLeft: 10,
    width: 200,
    top: -150,
    position: 'relative'
  }
}))

export default function BigCard ({
  type
}) {
  const classes = useStyles()
  return (
    <Link to={`/${type}`}>
      <Card className={classes[type]}>
        <Typography variant='h6' className={classes.text}>
          {type.toUpperCase()}
        </Typography>
        <div
          className={type === 'overthinking' ? classes.whiteLine : classes.blackLine}
        />
      </Card>
    </Link>
  )
}
